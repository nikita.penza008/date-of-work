import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

const VueInputMask = require('vue-inputmask').default;

new Vue({
  render: (h) => h(App),
}).$mount('#app');

Vue.use(VueInputMask);
