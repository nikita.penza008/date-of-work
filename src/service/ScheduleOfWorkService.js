import Vue from 'vue';
import { initializeApp } from 'firebase/app';
import {
  doc, getDoc, getFirestore, setDoc, collection, getDocs,
} from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyC4PcUszfNkUsN89_2ADsNki6wo3R3p2Hk',
  authDomain: 'schedule-of-work.firebaseapp.com',
  projectId: 'schedule-of-work',
  storageBucket: 'schedule-of-work.appspot.com',
  messagingSenderId: '291386053571',
  appId: '1:291386053571:web:6693f8038f2b7bafd66376',
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const AppealService = new Vue({
  methods: {
    async setAlteredCells(id, alteredCells) {
      await setDoc(doc(db, id, 'alteredCells'), alteredCells);
    },

    async getAlteredCells(id) {
      const alteredCells = await getDoc(doc(db, id, 'alteredCells'));
      return alteredCells.data();
    },

    async setSettings(id, settings) {
      await setDoc(doc(db, id, 'settings'), settings);
    },

    async getSettings(id) {
      const settings = await getDoc(doc(db, id, 'settings'));
      return settings.data();
    },

    async setUser(id, data) {
      await setDoc(doc(db, 'users', id), data);
    },

    async getAllUsers() {
      const allUsers = [];
      const users = await getDocs(collection(db, 'users'));
      users.forEach((user) => {
        allUsers.push(user.data());
      });
      return allUsers;
    },
  },
});

export default AppealService;
export { db };
